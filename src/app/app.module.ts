import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import {ReactiveFormsModule,FormsModule} from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CardComponent } from './components/card/card.component';
import { JeuxComponent } from './pages/jeux/jeux.component';
import { ContactComponent } from './pages/contact/contact.component';
import { InscriptComponent } from './pages/inscript/inscript.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfilComponent } from './pages/profil/profil.component';
import {AuthInterceptor} from './service/authconfig.interceptor';
import { TestComponent } from './pages/test/test.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';
import { AccesdeniedComponent } from './pages/accesdenied/accesdenied.component';
import { AdminComponent } from './pages/admin/admin.component';
import { PostTestComponent } from './pages/post-test/post-test.component'

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    HeaderComponent,
    FooterComponent,
    CardComponent,
    JeuxComponent,
    ContactComponent,
    InscriptComponent,
    LoginComponent,
    ProfilComponent,
    TestComponent,
    NotfoundComponent,
    AccesdeniedComponent,
    AdminComponent,
    PostTestComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
