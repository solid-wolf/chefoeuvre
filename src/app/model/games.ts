import { Url } from 'url';

export interface games{
    name: String;
    img: Url;
    desc: String;
}